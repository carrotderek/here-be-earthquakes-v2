var config = {
    countriesData: "../data/worldcountries.geo.json",
    quakesData: "../data/earthquakes_7days_2.5m_05082013.json",

    windowDimensions: 900,
    windowPadding: 50,
    arcWidth: -1,
        
    minColor: "#FFFF6E",
    maxColor: "#D42E08",

    radiusExp: 5,
    radiusExtent: [1,4],

    quakeHighlight: "#111",
    quakeHighlightWidth: 2
};