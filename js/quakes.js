(function() {

quakes = function(anchor) {
    var velocity = 0.005,
        then = Date.now(),
        earthquakes,
        magnitudeExtent,
        projection,
        path,
        svg,
        self = this;

    var magColorScale = d3.scale.linear()
        .domain([1, 7])
        .range([config.minColor, config.maxColor]);

    var tooltip = d3.select(anchor)
        .append("div")
        .attr("class", "tooltip");

    function initialize() {
        var globe = {type: "Sphere"};
        projection = d3.geo.orthographic()
            .scale(config.windowDimensions/2)
            .translate([
                (config.windowDimensions/2 + config.windowPadding/2),
                (config.windowDimensions/2 + config.windowPadding/2)
            ])
            .clipAngle(90);

        var zoom = d3.behavior.zoom()
            .translate(projection.translate())
            .scale(projection.scale())
            .scaleExtent([0, config.windowDimensions*10])
            .on("zoom", _zoom);

        path = d3.geo.path()
            .projection(projection);

        svg = d3.select(anchor).append("svg:svg")
            .attr("width", (config.windowDimensions + config.windowPadding))
            .attr("height", (config.windowDimensions + config.windowPadding))
            .call(zoom);

        svg.append("path")
            .datum(globe)
            .attr("class", "foreground")
            .attr("d", path);

        d3.json(config.countriesData, function(error, world) {
            svg.insert("path")
                .datum(topojson.object(world, world.objects.land))
                .attr("class", "land")
                .attr("d", path);

            drawQuakes(config.quakesData);

            d3.timer(function() {
                var angle = velocity * (Date.now() - then);

                projection.rotate([angle, 0, 0]);
                svg.selectAll("path")
                    .attr("d", path.projection(projection));

            });
        });

        return self;
    }

    function drawQuakes(source) {
  
        var circle = d3.geo.circle();
        d3.json(source, function(error, data) {

            magnitudeExtent = d3.extent(data.features, function(quake) {
                return quake.properties.mag;
            });

            quakeRadius = d3.scale.pow()
                .domain(magnitudeExtent)
                .range(config.radiusExtent)
                .exponent(config.radiusExp);

            earthquakes = svg.selectAll("quakes")
                .data(data.features)
                .enter()
                .append("path")
                .attr("fill", function(d){ return d3.rgb(magColorScale(d.properties.mag)).darker(0.0).toString(); })
                .attr("opacity", function(d){ return (1-(1-d.properties.mag*0.1)/2); })
                .attr("class", "quake")
                .on("mouseover", _quakeHover)
                .on("mouseout", _quakeOut)
                .datum(function(d) {
                    return circle.origin(
                        [d.geometry.coordinates[0], 
                        d.geometry.coordinates[1]]
                    )
                    .angle(quakeRadius(d.properties.mag))();
                })
        });

    }

    function _quakeHover(d) {
        earthquakes.attr("stroke", "none");
        selected = d3.select(this)
            .attr("stroke", config.quakeHighlight)
            .attr("stroke-width", config.quakeHighlightWidth);

            tooltip.classed("hidden", false)
                .attr("style", "left:" + d3.event.pageX + "px; top:" + d3.event.pageY + "px;")
                .html("hellloooooooo");
    }

    function _quakeOut(d) {
        selected = d3.select(this)
            .attr("stroke", "none");

        tooltip.attr("style", "display:none");
    }

    function _zoom() {
        projection.translate(d3.event.translate)
            .scale(d3.event.scale);
        svg.selectAll("path")
            .attr("d", path.projection(projection));
    }

    function _mouseDown() {
    }

    return initialize();
};

})();